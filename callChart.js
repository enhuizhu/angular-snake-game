  (function(obj){
    obj.callChart=function(config){
      /**
      * define the data first, and then bind the data to svg dom
      **/
      var svgWidth = config.svgWidth;
      var svgHeight = config.svgHeight;
      var w = config.chartWidth;
      var h = config.chartHeight;
      var r = config.circleRadius;
      var basicFontSize = config.basicFontSize;
      var textColor = config.textColor;
      var lineColor = config.lineColor;
      var circleColor = config.circleColor;
      var pathFillColor = config.pathFillColor;
      var chartWrapperSelector = config.chartWrapperSelector;
      var currentTimeRange = config.timeRange;
      var chartWrapper = d3.select(chartWrapperSelector);
      var selectLineColor="#999999";
      var callChart = null;
      var scale = null;
      var selectRange = null;
      var data = null
      var axisMinsArr = [];
      var that = this;
      
      /**
      * function convert time str to minuetes
      **/
      var convertTimeStrToMin = function(timeStr){
          var timeStrArr = timeStr.split(":");
          var hours = parseInt(timeStrArr[0]);
          var mins = parseInt(timeStrArr[1]);
          return hours*60+mins;
      }

      var initAxisMinArr = function(zoomIn){
          axisMinsArr=[];
          var startMins = convertTimeStrToMin(config.timeRange.from);
          var endMins = convertTimeStrToMin(config.timeRange.to);
          
          if (zoomIn) {
            var dividedNumber = 10;
          }else{
            var dividedNumber = 24;
          }

          var minDis = Math.floor((endMins-startMins)/dividedNumber);
          axisMinsArr.push(startMins);
          for(var i=1;i<=(dividedNumber-1);i++){
             axisMinsArr.push(startMins+i*minDis);
          }
          axisMinsArr.push(endMins);
      }


      var initDataAndScale = function(extenalData){
          data = extenalData;
          scale = d3.scale.linear().domain([0,d3.max(data)]).range([0,h-8]);
      }

      var bgLineData=function(data,dis){
         /**
         * generate background lines
         **/
         var lineData=[];
         var maxData = d3.max(data);
         var lineNumber = Math.ceil(maxData/dis);
         if(maxData%dis==0){
           lineNumber+=1;
         }
         for(var i=0;i<lineNumber;i++){
             lineData.push(i*dis);
         }
         return lineData;
      };

      /**
      * function to convert dots value to tow dementions array
      **/
      var convertDotToLineData = function(dotData, xDis,r,scale,h){
          var lineData = "";
          for(var i=0;i<dotData.length;i++){
             var x=i*xDis+r;
             var y=h-scale(dotData[i]);
             if (i==0) {
                lineData+="M"+x+" "+y;
             }else{
                lineData+=" L"+x+" "+y;
             };
          }   
           return lineData;
      };
      
      /**
      * bind different drag event
      **/
      var transformRight=function(offsetX,domObj){
          domObj.attr("transform","translate("+offsetX+",0)");
      }

      var transformLeft=function(offsetX,domObj){
           var pixelToMoveLeft = offsetX-svgWidth;
           domObj.attr("transform","translate("+pixelToMoveLeft+",0)");
      }
     

      var dragstart = function(){
          d3.event.sourceEvent.stopPropagation(); // silence other listeners
          /**
          * get start x and then move rect and line to the position
          **/
          that.startX = d3.event.sourceEvent.offsetX;  
      };
      
      var dragging = function(){
          var dx=d3.event.x - that.startX;
          var selectAreaOpacity = callChart.select(".select-area").attr("opacity");
          if(selectAreaOpacity==0){
              if(dx>=0){
                  callChart.selectAll(".start-rect,.start-line").call(function(){
                      transformRight(d3.event.x,this);
                   });
              }else{
                  callChart.selectAll(".end-line,.end-rect").call(function(){
                     transformLeft(d3.event.x,this);
                  });
              }
             callChart.select(".select-area").attr("opacity",1);
          }

          if(dx>=0){
            /**
            * move the right rect
            **/
             callChart.selectAll(".end-line,.end-rect").call(function(){
              transformLeft(d3.event.x,this);
             });
          }else{
            /**
            * move the left rect
            **/
            callChart.selectAll(".start-rect,.start-line").call(function(){
             transformRight(d3.event.x,this);
            });
          }
      };

      var dragend = function(){
          console.log("dragend",that.startX,that.endX);
          that.endX = d3.event.sourceEvent.offsetX;
          if(that.startX==that.endX && callChart.select(".select-area").attr("opacity")==1){
            initSelectArea();
          }else if (that.startX!=that.endX){
            setUpSelectRange(that.startX,that.endX);
            activeCirclesBaseOnRange(that.startX,that.endX);
            adjustSelection(that.startX,that.endX);
            displayZoomIn(that.startX,that.endX);
          }
      };


      /**
      * bind drag event for the whole svg
      **/
      var drag = d3.behavior.drag()
          .on("dragstart",dragstart)
          .on("drag",dragging)
          .on("dragend",dragend);

      /**
      * function to build svg element
      **/
      var buildSvg = function (){
          chartWrapper.append("svg").attr("width",svgWidth).attr("height",svgHeight).attr("id","callChart");
          callChart = d3.select("#callChart");
      };

       /**
      * function to build chart inside groups
      **/
      var buildGroups = function () {
          callChart.append("g").attr("class","bg-lines");
          callChart.append("g").attr("class","call-number");
          callChart.append("g").attr("class","lines");
          callChart.append("g").attr("class","dots");
          callChart.append("g").attr("class","texts");
          callChart.append("g").attr("class","select-area").attr("opacity","0");
          callChart.append("g").attr("id","call-detail").attr("opacity","0");
      };

      /**
      * function to draw background lines
      **/
      var drawBackgroundLinesAndText = function(extenalData){
          var bgData = bgLineData(extenalData,25);
          var bgLines = callChart.select(".bg-lines").selectAll("line").data(bgData);          
          bgLines.enter().append("line").attr("x1","0")
          .attr("y1",function(d,i){
           return h-scale(d);
          }).attr("x2",w).attr("y2",function(d,i){
             return h-scale(d); 
          }).attr("stroke",lineColor).attr("stroke-width","1");
          bgLines.exit().remove();

          var bgTexts = callChart.select(".call-number").selectAll("text").data(bgData);
          bgTexts.enter().append("text").attr("x",0).attr("y",function(d,i){
            return h-scale(d)+15; 
          }).attr("style","color:"+textColor+";font-size:"+basicFontSize).text(function(d,i){
            if(d==0) return "";
            return d;
          });
          bgTexts.exit().remove();
      };

      /**
      * funciton to draw the path
      **/
      var drawDataPath = function(extenalData){
          var lineData=convertDotToLineData(extenalData,w/(extenalData.length-1),r,scale,h);
          /**
          * should remove the old lines first
          **/
          callChart.selectAll(".lines path").remove();
          var lines = callChart.select(".lines").append("path").attr("d",lineData).attr("style","stroke:"+circleColor+";stroke-width:2;fill:"+pathFillColor);
      };

      /**
      * function to draw dots base on data
      **/
      var drawDotsBaseOnData = function(extenalData){
          var dots= callChart.select(".dots").selectAll("circle");
          var circles = dots.data(extenalData);
          circles.enter().append("circle").attr("cx",function(d,i){
                 return (w/(extenalData.length-1))*i+r; 
              }).attr("cy",function(d,i){
                 return h-scale(d);
              }).attr("r",r).attr("fill",circleColor).on("mouseover",function(d,i){
                var x = (w/(extenalData.length-1))*i+r+5;
                var y = h-scale(d)-48;

                /**
                * should check if y < 48
                **/
                if(y<0){
                   y=0;
                }

                /**
                * should check if x > svg.width
                **/
                if (x+100>w) {
                   x= w-100;
                };

                d3.select("#call-detail").attr("transform","translate("+x+","+y+")").attr("opacity",1).select("text").text(function(){
                    return d+" Calls";
                });
              }).on("mouseout",function(d,i){
                d3.select("#call-detail").attr("opacity",0);
              }).on("click",function(d,i){
                  console.log("click");
                  var newOpacity = callChart.select(".dots .out-circle-"+i).attr("opacity")==0?1:0;
                  callChart.select(".dots .out-circle-"+i).attr("opacity",newOpacity);
              });
          circles.exit().remove();

          var outCircles = dots.data(extenalData);
          outCircles.enter().append("circle").attr("cx",function(d,i){
                 return (w/(extenalData.length-1))*i+r; 
              }).attr("cy",function(d,i){
                 return h-scale(d);
              }).attr("r",r+3).
              attr("fill","none").
              attr("style","stroke-width:1;stroke:"+circleColor)
              .attr("opacity","0")
              .attr("class",function(d,i){
                return "out-circle-"+i;
              });

          outCircles.exit().remove();
      };

      var drawAxisTextBaseOnData = function(extenalData){
          var texts = callChart.select(".texts").selectAll("text").data(extenalData);
          texts.enter().append("text").attr("x",function(d,i){
          return (w/(extenalData.length-1))*i+r-15;
          }).attr("y",function(d,i){
            return h+31;
          }).attr("style","color:"+textColor+";font-size:"+basicFontSize+"px")
          .text(function(d,i){
              if(i%2==0) return "";
            var hours = i<10?"0"+i:i;
            var times = hours+":00";
            return times;
          });

          texts.exit().remove();
      };

      var buildCallDetailPop = function(){
           /**
           * set up the call detail
           **/
          var callDetailG = d3.select("#call-detail");
          callDetailG.append("rect").attr("x",0).attr("y",0).attr("width",100).attr("height",43).attr("style","stroke:"+lineColor+";stroke-width:1;fill:white");
          callDetailG.append("circle").attr("cx",15).attr("cy",21).attr("r",r).attr("fill",circleColor);
          callDetailG.append("text").attr("x",29).attr("y",26).attr("style","color:"+textColor+";font-size:"+(basicFontSize+2)+"px").text("50 Calls");
      };

      var buildSelectArea = function(){
          var selectAreaG = callChart.select(".select-area");
          selectAreaG.append("rect").attr("x",-svgWidth).attr("y",0).attr("width",svgWidth)
          .attr("height",h+r).attr("opacity",0.5).attr("fill","white").attr("class","start-rect");
          selectAreaG.append("line").attr("x1",0).attr("y1",h).attr("x2",0).attr("y2",0)
          .attr("stroke",selectLineColor).attr("stroke-width",1).attr("class","start-line");
          selectAreaG.append("line").attr("x1",svgWidth).attr("y1",h).attr("x2",svgWidth).attr("y2",0)
          .attr("stroke",selectLineColor).attr("stroke-width",1).attr("class","end-line");
          selectAreaG.append("rect").attr("x",svgWidth).attr("y",0).attr("width",svgWidth)
          .attr("height",h+r).attr("opacity",0.5).attr("fill","white").attr("class","end-rect");
      };

      var initSelectArea = function(){
           if (d3.event.defaultPrevented) return; // click suppressed
           hideAllOutCircles();
           hideZoom();
           selectRange = null;
           callChart.select(".select-area").selectAll(".start-rect,.end-rect,.start-line,.end-line")
           .attr("transform","translate(0,0)");
           callChart.select(".select-area").attr("opacity","0");
      };

      var hideAllOutCircles = function(){
           d3.select(".dots").selectAll("circle[class^=out-circle]").attr("opacity","0");
      };

      var setUpSelectRange = function (startX, endX){
          var dis=w/(data.length-1);
          var startPoint = Math.ceil(startX/dis);
          var endPoint = Math.floor(endX/dis);

          selectRange = {};
          selectRange.from = startPoint;
          selectRange.to = endPoint;
          selectRange.dis = dis;
          selectRange.leftX = dis*startPoint+r;
          selectRange.rightX = dis*endPoint+r;
          selectRange.middlePosition = startX+parseInt((endX-startX)/2);
      }

      var activeCirclesBaseOnRange = function (startX, endX){
          if (selectRange == null) return false;
          hideAllOutCircles();
          var selectors = "";
          for(var i=selectRange.from;i<=selectRange.to;i++){
              selectors+=".out-circle-"+i+",";
          }
          selectors=selectors.substr(0,selectors.length-1);
          d3.select(".dots").selectAll(selectors).attr("opacity",1);
      };

      var adjustSelection = function (startX,endX){
          if (selectRange == null) return false;
          transformLeft(selectRange.rightX,d3.selectAll(".end-rect,.end-line"));
          transformRight(selectRange.leftX,d3.selectAll(".start-rect,.start-line"));
      };

      var displayZoomIn = function (startX,endX){
          if (selectRange == null) return false;
          var positionX = selectRange.middlePosition;
          var operationWrapper = d3.select(".chart-control-panel .operation-wrapper");
          operationWrapper.style("left",positionX+"px");
          operationWrapper.classed("zoom-in",true);
          operationWrapper.classed("zoom-out",false);
      };


      var hideZoom = function(){
          var operationWrapper = d3.select(".chart-control-panel .operation-wrapper");
          operationWrapper.classed("zoom-in",false);
          operationWrapper.classed("zoom-out",false);
      };

      /**
      * function bind click event to zomm in icon
      **/
      var bindZoomInEvent = function(){
          d3.select(".chart-control-panel .operation-wrapper .glyphicon-zoom-in").on("click",function(){
                  console.log("select startPoint",selectRange.from);
                  console.log("select endPoint",selectRange.to);

           var newData = [10,20,30,40,50,60,70,100,90,80,70,150,75,48,9,50,23,28,74,99,10,45,60,80,100];
           initDataAndScale(newData);
           this.initChartBaseOnData(newData);
          });
      };

      this.initChartBaseOnData=function (extenalData){
          drawBackgroundLinesAndText(extenalData);
          drawDataPath(extenalData);
          drawDotsBaseOnData(extenalData); 
          drawAxisTextBaseOnData(extenalData);         
      }

      this.init = function(){
           initDataAndScale(config.callData);
           buildSvg();
           buildGroups();
           buildCallDetailPop();
           buildSelectArea();
           bindZoomInEvent();
           this.initChartBaseOnData(data);
           initAxisMinArr();
           /**
           * add drag event to whole svg
           **/
           callChart.call(drag);
      }

      this.init();      
  }
 }
)(window);