'use strict';

angular.module('frontendApp')
 .controller('NotifyCtrl', function(){
        var Controller = function($scope,notificationCentre,$timeout,Persist,$sce,PopupManager,$interval) {
              this.$scope = $scope;
              this.$timeout = $timeout;
              this.$sce = $sce;
              this.$interval = $interval;
              this.e = notificationCentre ;
              this.persist = Persist;
              this.popupManager = PopupManager;
              this.$scope.notifications = [];
              this.$scope.popups = [];
              this.lastMsg = this.persist.get("admin_msg") || {};
              this.$scope.errors = [];
              this.init.apply(this,[].slice.call(arguments,0));
        };
        Controller.prototype = {
            init : function($scope){
                var that = this;

                var pushNotification = function (msg, stack, delay) {

                    var exists = that.$scope[stack].filter(function (m) {

                        return (m.message == msg.message &&
                            m.isWarning == msg.isWarning &&
                            m.isSuccess == msg.isSuccess);

                    }).length > 0;

                    if (!exists) {
                        if (stack == 'popups') {
                            var conf = {
                                url: 'msgPopup.html#' + encodeURIComponent(msg.message),
                                title: 'msgPopup',
                                location: 'no',
                                width: 400
                            };
                            that.popupManager.openWin(conf);
                        } else {
                            delay = delay ? delay : 5;

                            msg.message = that.$sce.trustAsHtml(msg.message);
                            that.$scope[stack].unshift(msg);

                            that.$timeout(function () {
                                that.$scope[stack] = that.$scope.errors.slice(0, -1);
                            }, delay * 1000);
                        }
                    }
                };

                /**
                 * function to return seconds since agent hangup the call
                 */
                var getDispositionTime = function(serverTimeStamp){
                    var x = new Date();
                    var UTCseconds = (x.getTime() + x.getTimezoneOffset()*60*1000)/1000;
                    var serverSeconds =serverTimeStamp;
                    return Math.round(UTCseconds - serverSeconds);
                };


                /**
                 * function to update disposition time
                 */
                var updateDispositionTime = function(serverTimeStamp){
                     if(!that.dispositionUpdate && that.$scope.disposition){
                         that.$scope.dispositionTime = getDispositionTime(serverTimeStamp);
                         that.dispositionUpdate=that.$interval(function(){
                             that.$scope.dispositionTime = getDispositionTime(serverTimeStamp);
                             console.log("update disposition time",that.$scope.dispositionTime,serverTimeStamp);
                         },1000);
                     }
                };

                /**
                 * function to make warning popup appear in certain time period
                 */
                var registerTimeout = function(timePeriod,serverTimeStamp){
                    if(!that.displayWaring){
                        that.displayWaring = that.$interval(function(){
                            if(!that.$scope.disposition){
                                that.$scope.disposition = true;
                            }
                            
                            var dispositionTime = getDispositionTime(serverTimeStamp);

                            
                            alert("your are on disposition status for "+dispositionTime+" seconds");

                            

                            //updateDispositionTime(serverTimeStamp);
                        },timePeriod);
                    }
                };

                /**
                 * function to cancleWarning
                 */
                var cancelWaring = function(){
                    that.$interval.cancel(that.displayWaring);
                    that.displayWaring = null;
                }


                window.addEventListener("message",function(msg){
                   msg = angular.fromJson(msg.data);

                    if('e' in msg ){
                       that.$timeout(function(){
                           that.e.pub(msg.e,msg.data);
                       });
                    }
                });

                this.e.sub("frontend.notify.error",function(evt,data){
                    pushNotification({message:data[0]},'errors');
                });
                this.e.sub("frontend.notify.success",function(evt,data){
                    var msg = {message:data[0],isSuccess:true};
                    pushNotification(msg,'errors');
                });
                this.e.sub("frontend.notify.warning",function(evt,data){
                    var msg = {message:data[0],isWarning:true};
                    pushNotification(msg,'errors');
                });


                that.e.sub('backend.status', function(scope, args) {
                    var data = args[0];

                    /**
                     * should check if it has disposition check
                     */
                    if(data.disposition_check && data.disposition_check.disposition_enable == "Y"){
                        that.hangupTime = data.call.disconnect;
                        that.timeOutTime = data.disposition_check.disposition_time*1000;
                        //that.timeOutTime = 5000;
                        registerTimeout(that.timeOutTime,that.hangupTime);
                    }else{
                        if(that.displayWaring){
                            cancelWaring();
                            that.$scope.closeWarning();
                        }
                        that.$scope.disposition = false;
                    }


                    if(!angular.equals(that.lastMsg,data.broadcast_msgs) ){
                        angular.forEach(data.broadcast_msgs,function(v,k){
                            if(! (k in that.lastMsg)){
                                var stack = /^popup/.test(k) ? 'popups' : 'notifications';
                                pushNotification({message:v}, stack, 6000);
                            }
                        });
                        that.lastMsg = data.broadcast_msgs;
                        that.persist.set("admin_msg",that.lastMsg);
                    }

                });
                
                that.$scope.hideAdminMsg = function(item){
                    item.hidden = true;
                };
                that.$scope.closeItem = function(item){
                    var arr = that.$scope.errors,
                        i = arr.indexOf(item);
                    
                    arr.splice(i,1);
                    
                    that.$scope.errors = arr;
                };

                that.$scope.closeWarning = function(userClose){
                    that.$scope.disposition = false;
                    that.$interval.cancel(that.dispositionUpdate);
                    that.dispositionUpdate = null;
                    /**
                     * if the pop up waring is closed by user
                     * should register time out again
                     */
                    if(userClose){
                        cancelWaring();
                        registerTimeout(that.timeOutTime,that.hangupTime);
                    }
                };
            }
        };
    Controller.$inject = ['$scope','notificationCentre','$timeout','Persist','$sce','PopupManager','$interval'];
    return  Controller;
}());