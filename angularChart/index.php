<!doctype html>
<html>
   <head>
       <title> Chart Experiments</title>
       <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css"/>
       <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap-theme.min.css"/>
       <link rel="stylesheet" type="text/css" href="styles/callChart.css"/>
       <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css"/>
       <style type="text/css">
	     /* .chart-wrapper{
	      	 margin-left:auto;
	      	 margin-right: auto;
	      	 width: 956px;
          }*/
       </style>
   </head>
   <body ng-app="chart" ng-controller="main">
   <p>&nbsp;<p>
   <p>&nbsp;<p>
   <p>&nbsp;<p>
   <p>&nbsp;<p>
  

   <div class="container">
     <form ng-submit="getNewData()">
      <div class="row">
        <div class="col-lg-10 col-md-10">
            Start Time:
            <input type="text" id="startDatepicker"></input>

            &nbsp;&nbsp;
            End Time:
			
			<input type="text" id="endDatepicker"></input>  
             &nbsp; &nbsp;
            Time Gap:
            <select name ="timegap" id="timegap">
            	<option value="0">Please select time gap</option>
            	<option value="month">monthly</option>
            	<option value="day">dayly</option>
            	<option value="hour">hour</option>
            </select>
        </div>

        <div class="col-lg-2 col-md-2">
             <button class="btn btn-primary" type="submit"> Generate Chart </button>
        </div>
      </div>
      </form>
      <p>&nbsp;</p>
      <dxi-line-chart data="callData" codinate-x-labels="codinateXlabels" agents="agents" total="total">
     </dxi-line-chart>
      
      <div class="btn btn-primary" ng-click="addAgents()">Add Agent</div>
      <div class="btn btn-primary" ng-click="removeAgents()">Remove Agent</div>
      <div class="btn btn-primary" ng-click="changeCodinateXlabels()">change codinateX</div>
      <div class="btn btn-primary" ng-click="generateTotalAndCompeleteCalls()">Update Total</div>

   </div>

   <p>&nbsp;<p>
   <p>&nbsp;<p>
   <p>&nbsp;<p>


 <div class="container">
   <div class="row">
      
      <div class="col-lg-6 col-md-6">
          
          <pie data="pieChart"></pie>

      </div>

      <div class="col-lg-6 col-md-6">
            
            <div class="btn btn-primary" ng-click="addPieData()">Add Data</div>
            <p>&nbsp;</p>
            <div class="btn btn-primary" ng-click="removePieData()">Remove Data</div>

      </div>
    
   </div>
   

 </div>




   <script type="text/javascript" src="bower_components/angular/angular.min.js">
   </script>
   <script type="text/javascript" src="bower_components/angular-classy/angular-classy.min.js">
   </script>
   <script type="text/javascript" src="bower_components/d3/d3.min.js">
   </script>
   <script type="text/javascript" src="bower_components/jquery/dist/jquery.js">
   </script>
    <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.js">
   </script>
   <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>

    
   <!-- inlude angular app.js -->
   <script type="text/javascript" src="scripts/app.js"></script>
 
   <!-- include angular controller -->
   <script type="text/javascript" src="scripts/controllers/main.js"></script>

   <!-- include angular directives -->
   <script type="text/javascript" src="scripts/directives/dxiLineChart.js"></script>
   <script type="text/javascript" src="scripts/directives/pie.js"></script>
   <script type="text/javascript" src="scripts/directives/snake.js"></script>
   

   </body>
</html>