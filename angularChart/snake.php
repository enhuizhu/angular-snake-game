<!doctype html>
<html>
   <head>
       <title> Chart Experiments</title>
       <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css"/>
       <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap-theme.min.css"/>
       <link rel="stylesheet" type="text/css" href="styles/callChart.css"/>
       <style type="text/css">
	     /* .chart-wrapper{
	      	 margin-left:auto;
	      	 margin-right: auto;
	      	 width: 956px;
          }*/
       </style>
   </head>
   <body ng-app="chart" ng-controller="main">

   <p>&nbsp;<p>
   <p>&nbsp;<p>
   <p>&nbsp;<p>


 <div class="container">

   
    <div class="row">
        
          <snake></snake>

    </div>



 </div>




   <script type="text/javascript" src="bower_components/angular/angular.min.js">
   </script>
   <script type="text/javascript" src="bower_components/angular-classy/angular-classy.min.js">
   </script>
   <script type="text/javascript" src="bower_components/d3/d3.min.js">
   </script>
   <script type="text/javascript" src="bower_components/jquery/dist/jquery.js">
   </script>
    <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.js">
   </script>
   <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
    
   <!-- inlude angular app.js -->
   <script type="text/javascript" src="scripts/app.js"></script>
 
   <!-- include angular controller -->
   <script type="text/javascript" src="scripts/controllers/main.js"></script>

   <!-- include angular directives -->
   <script type="text/javascript" src="scripts/directives/lineChart.js"></script>
   <script type="text/javascript" src="scripts/directives/pie.js"></script>
   <script type="text/javascript" src="scripts/directives/snake.js"></script>
   

   </body>
</html>