/**
* directive for pie chart
**/

"use strict";

angular.module("chart").directive("pie",function(){
	return {
       restrict:"AE",
       replace:true,
       templateUrl:"./views/pie.html",
       scope:{
       	 "data":"="
       },
       link:function(scope,element,attr){
          
        scope.dataDetail={
          	 color:"#2392df",
          	 value:70,
          	 label:"",
          	 x:0,
          	 y:0
        }

        var colors = [
          "#2392df",
          "#ee9a14",
          "#27ae60",
          "#af5050",
          "#9550af",
          "#50afa7",
          "#afa550",
        ];

        scope.colors = colors;

        var generateRandomColor = function(){
            var getRandom = function(){
            	return Math.round(Math.random()*255);
            }
            var r = getRandom();
            var g = getRandom();
            var b = getRandom();
            return "rgb("+r+","+g+","+b+")";
        };

        var width = 360;
        var height = 360;
        var translateDis = 10;
        var radius = Math.min(width - translateDis * 2 , height - translateDis *2 ) / 2;
        
        var rootContainer = d3.select(element[0]).select("#dxiPie");
        
        var svg = rootContainer
          .attr('width', width)
          .attr('height', height)
          .insert('g',":first-child")
          .attr('transform', 'translate(' + (width / 2) + 
            ',' + (height / 2) + ')');

        var arc = d3.svg.arc()
          .outerRadius(radius)
          .innerRadius(30);

        var pie = d3.layout.pie()
          .value(function(d) { return d.count; })
          .sort(null);

       function buildPath(){

          svg.selectAll('path').remove();

          var paths = svg.selectAll('path')
          .data(pie(scope.data));
          
          paths.enter()
          .append("path")
          .transition()
          .duration(100)
          .delay(function(d,i){
          	 return 100*i;
          })
          .attr('d', function(d,i){
               return arc(d);
          })
          .attr('fill', function(d, i) { 
             if(!colors[i]) colors.push(generateRandomColor());
             return colors[i];
          })
          .each("end",function(d,i){
          	 d3.select(this)
          	 .on("mouseenter",function(d){
	          	 var angle = d.startAngle + (d.endAngle-d.startAngle)/2;
	          	 var translateY= - Math.round(translateDis * Math.cos(angle));
	             var translateX = Math.round(translateDis * Math.sin(angle));
	          	 d3.select(this)
	          	 .attr("transform","translate("+translateX+","+translateY+")");

                 rootContainer.select("#call-detail")
                 .classed("hideChartElement",false);
                 //.attr("transform","translate("+d3.event.offsetX+","+(d3.event.offsetY - 43)+")");

                 scope.$apply(function(){
                      scope.dataDetail.value = d.value;
                      scope.dataDetail.color = colors[i];
                      scope.dataDetail.label = scope.data[i].label;
                 });
	         })
	         .on("mouseleave",function(){
	         	 rootContainer.select("#call-detail").classed("hideChartElement",true);
	          	 d3.select(this).attr("transform","translate(0,0)");
	         });
          });

          paths.exit().remove();
          
       };

      scope.$watch("data",function(newVal,oldVal){
          buildPath();
      },true);

	}
  }
});
