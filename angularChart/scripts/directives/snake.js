"use strict";

angular.module("chart").directive("snake",function($timeout){

   return {
   	  restrict : "AE",
      replace : true,
      templateUrl : "./views/snake.html",
      link : function($scope,element,attr){
        
          
          var game = {

          	  config:{
                  fruitColor:"green",
                  sankeBodyColor:[
                    "#2392df",
                    "#ee9a14",
                    "#27ae60",
                    "#af5050",
                    "#9550af",
                    "#50afa7",
                    "#afa550",
                  ],
                  fruitSize : 10,
                  bodyNodeSize : 20,
                  canvasWidth:20,
                  canvasHeight:20
          	  },

              directions:{
                left: 37,
                right: 39,
                up: 38,
                down: 40,
              },

              currentDirection : null,
              
              scene : d3.select(element[0]).select("svg"),

              speed : 500,

              xstep : 1,

              ystep : 0,
              
              t: null,

              gameStarted : false,
              
              pauseGame : function(){
                 $timeout.cancel(this.t);
              },
              
              continueGame : function(){
                 this.updateScence();
              },

              getBodyColor : function(index){
                  var getRandom = function(){
                      return Math.round(Math.random()*255);
                  }

                  if(!this.config.sankeBodyColor[index]){
                     var r = getRandom();
                     var g = getRandom(); 
                     var b = getRandom();
                     this.config.sankeBodyColor.push("rgb("+r+","+g+","+b+")");
                  }

                  return this.config.sankeBodyColor[index];
              },

              bindKeyDownEvents : function(add){
                  var that  = this;
                  this.keyDownInited = add;
                  add ? 
                      document.addEventListener("keydown", this.directionControl.bind(this))
                      : 
                      document.removeEventListener("keydown",this.directionControl.bind(this));
              },

              initEventsListeners:function(){
                  //this.keyDownInited = true;
                  this.bindKeyDownEvents(true);
                  
                  var that  = this;
                  
                  document.addEventListener("click",function(e){
                       
                       if(e.target.classList.contains("snake") && !that.keyDownInited){
                            that.bindKeyDownEvents(true);
                       }else if(!e.target.classList.contains("snake") && that.keyDownInited){
                            that.bindKeyDownEvents(false);
                       }
                  });
              },

              directionControl:function(e){
                   
                   var ishorizontal = this.currentDirection == this.directions.left
                              || this.currentDirection == this.directions.right;

                   var isvertical = this.currentDirection == this.directions.up
                              || this.currentDirection == this.directions.down;
                   

                   switch (e.keyCode){
                       case this.directions.left:
                            if(ishorizontal) return false;
                            this.currentDirection = this.directions.left;
                            this.xstep = -1;
                            this.ystep = 0
                            break;
                       case this.directions.right:
                            if(ishorizontal) return false;
                            this.currentDirection = this.directions.right;
                            this.xstep = 1;
                            this.ystep = 0;
                            break;
                       case this.directions.up:
                            if (isvertical) return false;
                            this.currentDirection = this.directions.up;
                            this.ystep = -1;
                            this.xstep = 0;
                            break;
                       case this.directions.down:
                            if (isvertical) return false;
                            this.currentDirection = this.directions.down;
                            this.ystep = 1;
                            this.xstep = 0;
                            break;
                       case 65:
                            this.addSnakeNode();
                            break;
                       default:
                            break;     
                   }

              },

              createSnakeBody : function(){
                  var that = this;
                  this.scene.select(".snake-body")
                  .selectAll("rect")
                  .data([1,2,3])
                  .enter()
                  .append("rect")
                  .attr("x",function(d,i){
                       return i*that.config.bodyNodeSize;
                  })
                  .attr("y",0)
                  .attr("width",that.config.bodyNodeSize)
                  .attr("height",that.config.bodyNodeSize)
                  .attr("fill",function(d,i){
                    return that.getBodyColor(i);
                  });
              },

              addSnakeNode : function(){
                  /**
                  * get the information of tail node
                  **/
                  var currentSnakeLength = this.scene.select(".snake-body").selectAll("rect")[0].length;
                  var tailNode = this.scene.select(".snake-body")
                  .select("rect:first-child");
                  
                  var tailX = tailNode.attr("x");
                  var tailY = tailNode.attr("y");
                  
                  var newX = null;
                  var newY = null;

                  switch(this.currentDirection){
                     case this.directions.left:
                          newY = tailY;
                          newX = tailX + this.config.bodyNodeSize;   
                          break;
                     case this.directions.right:
                          newY = tailY;
                          newX = tailX - this.config.bodyNodeSize;
                          break;
                     case this.directions.up:
                          newX = tailX;
                          newY = tailY + this.config.bodyNodeSize;
                          break;
                     case this.directions.down:
                          newX = tailX;
                          newY = tailY - this.config.bodyNodeSize;
                          break;
                  }
                  
                  this.scene.select(".snake-body").insert("rect",":first-child")
                  .attr("x",newX)
                  .attr("y",newY)
                  .attr("width",this.config.bodyNodeSize)
                  .attr("height",this.config.bodyNodeSize)
                  .attr("fill",this.getBodyColor(currentSnakeLength));

              },
             
              /**
              * chang the postion of fruit
              **/
              generateFruitRnadomPostion: function(){
                 
                 var that = this;

                 var getXY = function(){
                    
                     var x = (Math.round(Math.random()*(that.config.canvasWidth-1))+0.5)*that.config.bodyNodeSize
                     var y = (Math.round(Math.random()*(that.config.canvasHeight-1))+0.5)*that.config.bodyNodeSize

                     return {
                       x : x,
                       y : y
                     }
                 }
                 
                 var isValid = false;
                 var pos = getXY();
                 /**
                 * check if x,y is valid value 
                 **/
                 var nodes = this.scene.select(".snake-body").selectAll("rect")[0];
                 
                 while(!isValid){
                    for(var i =0 ; i < nodes.length; i++){
                     
                     var currentX = parseInt(d3.select(nodes[i]).attr("x"));
                     var currentY = parseInt(d3.select(nodes[i]).attr("y"));
                     
                     var isInXRange = pos.x > currentX && pos.x < currentX + this.config.bodyNodeSize;
                     var isInYRange = pos.y > currentY && pos.y < currentY + this.config.bodyNodeSize;

                     if (isInXRange && isInYRange) {
                         pos = getXY();
                         break;
                     };

                     if(i == nodes.length -1){
                        isValid = true;
                     }
                   }
                 }

                 return pos;
              },

              isFruitTouchNode : function(obj2){

                 var fruit = this.scene.select(".fruit").select("#fruit");
                 var pos = {
                     x: parseInt(fruit.attr("cx")),
                     y: parseInt(fruit.attr("cy"))
                 }
                 
                 var currentX = parseInt(obj2.attr("x"));  
                 var currentY = parseInt(obj2.attr("y"));

                 var isInXRange = pos.x > currentX && pos.x < currentX + this.config.bodyNodeSize;
                 var isInYRange = pos.y > currentY && pos.y < currentY + this.config.bodyNodeSize;   
                 

                 if (isInXRange && isInYRange) {
                     return true;       
                 }
                 return false;
              },


              createFruit : function(add){
                 var fruit = this.scene.select(".fruit");
                 if(add){
                    fruit.append("circle")             
                    .attr("id","fruit")
                    .attr("cx",70)
                    .attr("cy",10)
                    .attr("r",this.config.fruitSize)
                    .attr("fill",this.config.fruitColor);
                 }else{
                    var pos = this.generateFruitRnadomPostion();
                    fruit.select("#fruit")
                    .attr("cx",pos.x)
                    .attr("cy",pos.y);
                 }
              },




              updateScence : function(){
                  
                  var that  = this;
                  var snakeNodes = this.scene.select(".snake-body").selectAll("rect")[0];
                  var totalNodes = snakeNodes.length;
                  var firstNode = d3.select(snakeNodes[totalNodes-1]);
                  
                  var lastPosition = {
                      x : parseInt(firstNode.attr("x")) + this.xstep * this.config.bodyNodeSize,
                      y : parseInt(firstNode.attr("y")) + this.ystep * this.config.bodyNodeSize
                  }

                  if (lastPosition.x >=this.config.canvasWidth*this.config.bodyNodeSize) {lastPosition.x = 0;};
                  if (lastPosition.x <0) {lastPosition.x = this.config.canvasWidth*this.config.bodyNodeSize - this.config.bodyNodeSize;};
                  if (lastPosition.y >=this.config.canvasHeight*this.config.bodyNodeSize) {lastPosition.y = 0;};
                  if (lastPosition.y <0) {lastPosition.y = this.config.canvasHeight*this.config.bodyNodeSize - this.config.bodyNodeSize;};
                
                  for(var i = totalNodes -1; i>=0;i--){
                     d3.select(snakeNodes[i])
                     .attr("x",function(){
                        var currentX = d3.select(this).attr("x");
                        var posX = lastPosition.x;
                        lastPosition.x = currentX;
                        return posX;
                     })
                     .attr("y",function(){
                        var currentY = d3.select(this).attr("y");
                        var posY = lastPosition.y
                        lastPosition.y = currentY;
                        return posY;
                     });
                  }

                   if(this.isFruitTouchNode(firstNode)){
                      this.createFruit();
                      this.addSnakeNode();
                   }

                  //return ;

                  this.t = $timeout(function(){
                     that.updateScence(); 
                  },this.speed);
                
              },

              initCanvas : function(){
                 var that = this;
                 this.scene
                 .attr("width",this.config.canvasWidth * this.config.bodyNodeSize)
                 .attr("height",this.config.canvasHeight * this.config.bodyNodeSize)
                 .attr("style","border:solid 1px gray")
                 .on("click",function(){
                      // return false;
                      if(!that.gameStarted){
                         that.updateScence();
                         that.gameStarted = true;
                      }
                      if(this.keyDownInited) return false;
                      that.initEventsListeners();
                 });
                 this.currentDirection = this.directions.right;
                 this.createFruit(true);
                 this.createSnakeBody();
              }
          
          }


          game.initCanvas();
          
          $scope.addNode = function(){
              game.addSnakeNode();
          };
          
          $scope.pauseGame = function(){
              game.pauseGame();
          };

          $scope.continueGame = function(){
              game.continueGame();
          };

          $scope.changeFruitPos = function(){
              game.createFruit();
          };

          $scope.updateScence = function(){
              game.updateScence();
          }
      }
   }

});