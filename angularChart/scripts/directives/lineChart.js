"use strict";

angular.module("chart").directive("lineChart",function($timeout){
    
    return {
       restrict:"AE",
       
       replace: true,
       
       templateUrl:"./views/lineChart.html",

       scope:{
         "data":"=",
         "codinateXLabels":"=",
         "agents":"=",
       },
       
       link:function($scope,element,attr){
       	 var config = {
              svgWidth:956,
              svgHeight:400,
              chartWidth:947,
              chartHeight:250,
              circleRadius:4,
              textColor:"#666666",
              lineColor:"#d8dadc",
              circleColor:"#2392df",
              pathFillColor:"#e8f3fb",
              basicFontSize:12,
              chartWrapperSelector:".chart-wrapper"
         }
         /**
        * define the data first, and then bind the data to svg dom
        **/
        var xPaddingLeft = 20;
        var svgWidth = config.svgWidth + xPaddingLeft*2;
        var svgHeight = config.svgHeight;
        var w = config.chartWidth;
        var h = config.chartHeight;
        var r = config.circleRadius;
        var basicFontSize = config.basicFontSize;
        var textColor = config.textColor;
        var lineColor = config.lineColor;
        var circleColor = config.circleColor;
        var pathFillColor = config.pathFillColor;
        var chartWrapperSelector = config.chartWrapperSelector;
        var currentTimeRange = config.timeRange;
        var chartWrapper = d3.select(element[0]);
        var selectLineColor="#999999";
        var callChart = chartWrapper.select("#callChart");
        var context
        var scale = null;
        var contextScaleY = null;
        var selectRange = null;
        var data = null
        var axisMinsArr = [];
        var that = this;
        //var codinateXLabels= $scope.codinateXLabels;
        var contextHeight = 50;
        var annimationDuration = 2000;
        var barWidth = 15;
        var context = chartWrapper.select("svg").select(".context")
              .attr("transform","translate(0,"+(h+70)+")");
        var dragData = {
            
        };

        var xscale = d3.time.scale().range([xPaddingLeft,svgWidth-xPaddingLeft*5]);
        var xscale2 = d3.time.scale().range([xPaddingLeft,svgWidth-xPaddingLeft]);

        var xAxis = d3.svg.axis().scale(xscale).orient("bottom");
        var xAxis2 = d3.svg.axis().scale(xscale).orient("bottom");

        var area = d3.svg.area()
            .interpolate("monotone")
            .x(function(d,i){
               return xscale2(new Date($scope.codinateXLabels[i]));
            })
            .y0(contextHeight)
            .y1(function(d,i){
                return contextHeight - contextScaleY(d);
            });

        var line = d3.svg.line()
            .interpolate("cardinal")
            .x(function(d,i){
               return xscale(new Date($scope.codinateXLabels[i]));
            })
            .y(function(d,i){
               return h - scale(d);
            });
         

        var colors = [
          "#2392df",
          "#ee9a14",
          "#27ae60",
          "#af5050",
          "#9550af",
          "#50afa7",
          "#afa550",
        ];

        $scope.colors=colors;
        $scope.chartType = "line";
        // $scope.svgWidth = svgWidth;
        // $scope.svgHeight = svgHeight;

       var getX = function(index,extenalData){
             var xPos = index % (extenalData.length);
             try{
                return xscale(new Date($scope.codinateXLabels[xPos]));
             }catch(e){
                return 0;
             }
       };

       var initDataAndScale = function(extenalData){
          data = angular.copy(extenalData);
          initXais();
          var allData = d3.merge(data);
          contextScaleY = d3.scale.linear()
                            .range([0,contextHeight])
                            .domain([0,d3.max(allData)]);
          scale = d3.scale.linear().domain([0,d3.max(allData)]).range([8,h-8]);
       }

       var initXais = function(){
           console.log("value of codinateXLabels in initXais",$scope.codinateXLabels);
           xscale.domain(d3.extent($scope.codinateXLabels.map(function(d) { return new Date(d);})));
           xscale2.domain(xscale.domain());            
           callChart.select(".x.axis").call(xAxis);
           context.select(".x.axis").call(xAxis2);   
       }




       var bgLineData=function(data,dis){
         /**
         * generate background lines
         **/
         var lineData=[];
         var maxData = d3.max(d3.merge(data));
         var lineNumber = Math.ceil(maxData/dis);
         if(maxData%dis==0){
           lineNumber+=1;
         }
         for(var i=0;i<=lineNumber;i++){
             lineData.push(i*dis);
         }
         return lineData;
       };


       /**
       * function to convert dots value to tow dementions array
       **/
       var convertDotToLineData = function(dotData, xDis,r,scale,h){
          var lineData = "";
          var totalNumberOfData = dotData.length;
          for(var i=0;i<totalNumberOfData;i++){
             var x = getX(i,dotData);
             var y = h-scale(dotData[i]);
             
             if (i==0) {
                lineData+="M"+x+" "+y;
             }else{
                lineData+=" L"+x+" "+y;
             };
          }   
           return lineData;
       };
       
      /**
      * bind mouse over function 
      **/
      var chartMouseOver = function(extenalData,i,d,index){
          var index = index? index : Math.floor(i/extenalData.length);
          var x = getX(i,extenalData)+r*2;
          var y = h-scale(d)-48;
          /**
          * should check if y < 48
          **/
          if(y<0){
             y=0;
          }

          d3.select("#call-detail")
          .attr("transform","translate("+x+","+y+")")
          .classed("hideChartElement",false)
          .select("text").text(function(){
              return d+" Calls";
          });
          
          d3.select("#call-detail").select("circle").attr("fill",colors[index]);
      }

      var chartMouseOut = function(){
           d3.select("#call-detail").classed("hideChartElement",true);
      }


      /**
      * bind different drag event
      **/
      var transformRight=function(offsetX,domObj){
          domObj.attr("transform","translate("+offsetX+",0)");
      }

      var transformLeft=function(offsetX,domObj){
           var pixelToMoveLeft = offsetX-svgWidth;
           domObj.attr("transform","translate("+pixelToMoveLeft+",0)");
      }

      var dragstart = function(){
          d3.event.sourceEvent.stopPropagation(); // silence other listeners
          /**
          * get start x and then move rect and line to the position
          **/
          dragData.startX = d3.event.sourceEvent.offsetX;  
      };

      var dragging = function(){
          var dx=d3.event.x - dragData.startX;
          var selectAreaOpacity = callChart.select(".select-area").attr("opacity");
          if(selectAreaOpacity==0){
              if(dx>=0){
                  callChart.selectAll(".start-rect,.start-line").call(function(){
                      transformRight(d3.event.x,this);
                   });
              }else{
                  callChart.selectAll(".end-line,.end-rect").call(function(){
                     transformLeft(d3.event.x,this);
                  });
              }
             callChart.select(".select-area").attr("opacity",1);
          }

          if(dx>=0){
            /**
            * move the right rect
            **/
             callChart.selectAll(".end-line,.end-rect").call(function(){
              transformLeft(d3.event.x,this);
             });
          }else{
            /**
            * move the left rect
            **/
            callChart.selectAll(".start-rect,.start-line").call(function(){
             transformRight(d3.event.x,this);
            });
          }
      };


      var dragend = function(){
          dragData.endX = d3.event.sourceEvent.offsetX;
          if(dragData.startX==dragData.endX && callChart.select(".select-area").attr("opacity")==1){
            initSelectArea();
          }else if (dragData.startX!=dragData.endX){
            setUpSelectRange(dragData.startX,dragData.endX);
            activeCirclesBaseOnRange(dragData.startX,dragData.endX);
            adjustSelection(dragData.startX,dragData.endX);
            displayZoomIn(dragData.startX,dragData.endX);
          }
      };

      /**
      * bind drag event for the whole svg
      **/
      var drag = d3.behavior.drag()
          .on("dragstart",dragstart)
          .on("drag",dragging)
          .on("dragend",dragend);

      /**
      * function to build svg element
      **/
      var buildSvg = function (){
          d3.select(element[0]).select("svg").attr("width",svgWidth).attr("height",svgHeight);
          callChart.append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + h + ")")
          .call(xAxis);
      };

      /**
      * function to build chart inside groups
      **/
      var buildGroups = function () {
          return false;
          callChart.append("g").attr("class","bg-lines");
          callChart.append("g").attr("class","call-number");
          callChart.append("g").attr("class","lines");
          callChart.append("g").attr("class","dots");
          callChart.append("g").attr("class","bar-group");
          callChart.append("g").attr("class","texts");
          callChart.append("g").attr("class","select-area").attr("opacity","0");
          callChart.append("g").attr("id","call-detail").classed("hideChartElement",true);
      };

      /**
      * function to draw background lines
      **/
      var drawBackgroundLinesAndText = function(extenalData){
          var bgData = bgLineData(extenalData,25);
          callChart.select(".bg-lines").selectAll("line").remove();
          var bgLines = callChart.select(".bg-lines").selectAll("line").data(bgData);          
          bgLines.enter().append("line").attr("x1","0")
          .attr("y1",function(d,i){
           return h-scale(d);
          }).attr("x2",w+xPaddingLeft*2).attr("y2",function(d,i){
             return h-scale(d); 
          }).attr("stroke",lineColor).attr("stroke-width","1");
          bgLines.exit().remove();
          
          callChart.select(".call-number").selectAll("text").remove();

          var bgTexts = callChart.select(".call-number").selectAll("text").data(bgData);
          bgTexts.enter().append("text").attr("x",0).attr("y",function(d,i){
            return h-scale(d)+15; 
          }).attr("style","color:"+textColor+";font-size:"+basicFontSize).text(function(d,i){
            if(d==0) return "";
            return d;
          });
          bgTexts.exit().remove();
      };

      /**
      * funciton to draw the path
      **/
      var drawDataPath = function(extenalData,noAnimation){
          
          var animationTime = noAnimation ? 0 : annimationDuration;

          /**
          * should remove the old lines first
          **/
          callChart.selectAll(".lines path").remove();
          callChart.select(".dots").selectAll("circle").remove();
          angular.forEach(extenalData,function(value,key){
              //var lineData=convertDotToLineData(value,w/(value.length-1),r,scale,h);
              var path = callChart.select(".lines").append("path")
              .attr("d",line(value))
              .attr("style","stroke:"+colors[key]+";stroke-width:2;fill:transparent")
              .attr("class","path"+key);

              var totalLength = path.node().getTotalLength();
              
              path
              .attr("stroke-dasharray",totalLength + " " + totalLength)
              .attr("stroke-dashoffset",totalLength)
              .transition()
               .duration(animationTime)
               .ease("liner")
               .attr("stroke-dashoffset",0);
          });

          $timeout(function(){
             drawDotsBaseOnData(extenalData);
          },animationTime)
          
      };

      /**
      * function to draw dots base on data
      **/
      var drawDotsBaseOnData = function(extenalData){
          //callChart.select(".dots").selectAll("circle").remove();
          
          if (extenalData.length<=0) {return false;};
          var allData = d3.merge(extenalData);
          var dots= callChart.select(".dots").selectAll("circle");
          var circles = dots.data(allData);
          
          circles.enter().append("circle")
              .attr("cx",function(d,i){
                 return getX(i,extenalData[0]);
              }).attr("cy",function(d,i){
                 return h-scale(d);
              }).attr("r",r).attr("fill",function(d,i){
                  var index = Math.floor(i/extenalData[0].length);
                  return colors[index];
              }).attr("class",function(d,i){
                  var index = Math.floor(i/extenalData[0].length);
                  return "dot-"+index;
              }).on("mouseover",function(d,i){
                  chartMouseOver(extenalData[0],i,d);
              }).on("mouseout",function(d,i){
                  chartMouseOut();
              }).on("click",function(d,i){
                  var newOpacity = callChart.select(".dots .out-circle-"+i).attr("opacity")==0?1:0;
                  callChart.select(".dots .out-circle-"+i).attr("opacity",newOpacity);
              });
          circles.exit().remove();

          var outCircles = dots.data(allData);
          outCircles.enter().append("circle").attr("cx",function(d,i){
                 return getX(i,extenalData[0]);
              }).attr("cy",function(d,i){
                 return h-scale(d);
              }).attr("r",r+3).
              attr("fill","none").
              attr("style",function(d,i){
                var index = Math.floor(i/extenalData[0].length);
                return "stroke-width:1;stroke:"+colors[index];
              })
              .attr("opacity","0")
              .attr("class",function(d,i){
                var index = Math.floor(i/extenalData[0].length);
                return "out-circle-"+i+" dot-"+index;
              });
          outCircles.exit().remove();
      };

      /**
      * function to draw bar chart
      **/
      var drawBars = function(extenalData,noAnimation){
          
          var animationTime = noAnimation ? 0 : annimationDuration;
          
          callChart.select(".bar-group").selectAll("rect").remove();
          
          if (extenalData.length<=0) { return false;};
          
          var dataLength = extenalData[0].length;
          
          angular.forEach(extenalData,function(v1,k1){
                angular.forEach(v1,function(v2,k2){
                      callChart.select(".bar-group").append("rect")
                       .attr("x",function(){
                           return xscale(new Date($scope.codinateXLabels[k2])) + k1*barWidth -3
                      })
                      .attr("width",barWidth)
                      .attr("style","fill:"+colors[k1])
                      .on("mouseover",function(){
                           chartMouseOver(v1,k2,v2,k1);
                      })
                      .on("mouseout",function(d,i){
                           chartMouseOut();
                      })
                      .attr("height",0)
                      .attr("y",h-8)
                      .transition()
                       .duration(animationTime)
                       .ease("liner")
                       .attr("height",function(){
                           return scale(v2);
                       })
                       .attr("y",function(){
                           return h-8-scale(v2);
                      });
                      
            });
        });
       };


      /**
      * brush events here
      **/


      var brush = d3.svg.brush()
      .x(xscale2)
      .on("brush", function(){
           var extent = brush.extent();
          xscale.domain(brush.empty()? xscale2.domain() : extent);
          var context = chartWrapper.select("svg").select(".context");
          var leftBrushRect = context.select(".outside-brush").select("rect:first-child");
          var rightBrushRect = context.select(".outside-brush").select("rect:last-child");
          
          if(brush.empty()){
            transformRight(0,leftBrushRect);
            transformLeft(svgWidth,rightBrushRect);
          }else{
            transformRight(xscale2(extent[0]),leftBrushRect);
            transformLeft(xscale2(extent[1]),rightBrushRect);
          }

          drawDataPath($scope.data,true);
          //drawDotsBaseOnData($scope.data,true);
          drawBars($scope.data,true);
          callChart.select(".x.axis").call(xAxis);

      })
      .on("brushend",function(){
          // console.log("the value of brush is:",brush.extent());
          // console.log("brush finish");
          
      });

      /**
      * function to draw extent area
      **/
      var drawExtent = function (extenalData){

          context.append("line")
          .attr("x1",0)
          .attr("y1",0)
          .attr("x2",svgWidth)
          .attr("y2",0)
          .attr("style","stroke:#d2d2d2");

          context.append("line")
          .attr("x1",0)
          .attr("y1",contextHeight)
          .attr("x2",svgWidth)
          .attr("y2",contextHeight)
          .attr("style","stroke:#d2d2d2");

          
          context.append("g")
          .attr("class","chart");

          context.append("g")
          .attr("class","lines")
          .selectAll("line")
          .data($scope.codinateXLabels)
          .enter()
          .append("line")
          .attr("x1",function(d,i){
            return xscale2(new Date(d));
          })
          .attr("y1",0)
          .attr("x2",function(d,i){
            return xscale2(new Date(d))
          })
          .attr("y2",contextHeight)
          .attr("style","stroke:#d2d2d2")
          .attr("stroke-dasharray","8,8");

          context.append("g")
          .attr("class","outside-brush")
          .selectAll("rect")
          .data([-1,1])
          .enter()
          .append("rect")
          .attr("x",function(d,i){
            return d*svgWidth
          })
          .attr("y",0)
          .attr("width",svgWidth)
          .attr("height",contextHeight)
          .attr("style","fill:rgba(255,255,255,0.5)");


          context.append("g")
          .attr("class", "x brush")
          .call(brush)
          .selectAll("rect")
          .attr("y", 0)
          .attr("height", contextHeight)
          .attr("fill","transparent");

          
          context.append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + (contextHeight-5) + ")")
          .call(xAxis2);
          drawExtentChart(extenalData);

      };


      var drawExtentChart = function(extenalData){
          chartWrapper.select("svg").select(".context")
          .select(".chart").selectAll("path").remove();
          angular.forEach(extenalData,function(value,key){
                chartWrapper.select("svg")
                .select(".context")
                .select(".chart")
                .append("path")
                .datum(value)
                .attr("class","area")
                .attr("d",area)
                .attr("style","fill:"+colors[key]);
          });
      };

      var drawAxisTextBaseOnData = function(extenalData){
          if(extenalData.length <= 0) return false;
          codinateXLabels=angular.copy($scope.codinateXLabels);
          callChart.select(".texts").selectAll("text").remove();
          var texts = callChart.select(".texts").selectAll("text").data(extenalData[0]);
          texts.enter().append("text").attr("x",function(d,i){
          return (w/(extenalData[0].length-1))*i+r+xPaddingLeft-15;
          }).attr("y",function(d,i){
            return h+31;
          }).attr("style","color:"+textColor+";font-size:"+basicFontSize+"px")
          .text(function(d,i){
            return codinateXLabels[i];
          });

          texts.exit().remove();
      };

      var buildCallDetailPop = function(){
           /**
           * set up the call detail
           **/
          var callDetailG = callChart.select("#call-detail");
          callDetailG.append("rect").attr("x",0).attr("y",0).attr("width",100).attr("height",43).attr("style","stroke:"+lineColor+";stroke-width:1;fill:white");
          callDetailG.append("circle").attr("cx",15).attr("cy",21).attr("r",r).attr("fill",circleColor);
          callDetailG.append("text").attr("x",29).attr("y",26).attr("style","color:"+textColor+";font-size:"+(basicFontSize+2)+"px").text("50 Calls");
      };

      var buildSelectArea = function(){
          var selectAreaG = callChart.select(".select-area");
          selectAreaG.append("rect").attr("x",-svgWidth).attr("y",0).attr("width",svgWidth)
          .attr("height",h+r).attr("opacity",0.5).attr("fill","white").attr("class","start-rect");
          selectAreaG.append("line").attr("x1",0).attr("y1",h).attr("x2",0).attr("y2",0)
          .attr("stroke",selectLineColor).attr("stroke-width",1).attr("class","start-line");
          selectAreaG.append("line").attr("x1",svgWidth).attr("y1",h).attr("x2",svgWidth).attr("y2",0)
          .attr("stroke",selectLineColor).attr("stroke-width",1).attr("class","end-line");
          selectAreaG.append("rect").attr("x",svgWidth).attr("y",0).attr("width",svgWidth)
          .attr("height",h+r).attr("opacity",0.5).attr("fill","white").attr("class","end-rect");
      };

      var initSelectArea = function(){
           try{
              if (d3.event.defaultPrevented) return; // click suppressed
           }catch(e){
              console.error(e.message);
           }
           hideAllOutCircles();
           hideZoom();
           selectRange = null;
           callChart.select(".select-area").selectAll(".start-rect,.end-rect,.start-line,.end-line")
           .attr("transform","translate(0,0)");
           callChart.select(".select-area").attr("opacity","0");
      };

      var hideAllOutCircles = function(){
           d3.select(".dots").selectAll("circle[class^=out-circle]").attr("opacity","0");
      };

      var setUpSelectRange = function (startX, endX){
          var dis=w/(data[0].length-1);
          var startPoint = Math.ceil(startX/dis);
          var endPoint = Math.floor(endX/dis);

          selectRange = {};
          selectRange.from = startPoint;
          selectRange.to = endPoint;
          selectRange.dis = dis;
          selectRange.leftX = dis*startPoint+r+xPaddingLeft;
          selectRange.rightX = dis*endPoint+r+xPaddingLeft;
          selectRange.middlePosition = startX+parseInt((endX-startX)/2);
      }

       var activeCirclesBaseOnRange = function (startX, endX){
          if (selectRange == null) return false;
          hideAllOutCircles();
          var selectors = "";
          for(var i=selectRange.from;i<=selectRange.to;i++){
              selectors+=".out-circle-"+i+",";
          }
          selectors=selectors.substr(0,selectors.length-1);
          d3.select(".dots").selectAll(selectors).attr("opacity",1);
      };

      var adjustSelection = function (startX,endX){
          if (selectRange == null) return false;
          transformLeft(selectRange.rightX,d3.selectAll(".end-rect,.end-line"));
          transformRight(selectRange.leftX,d3.selectAll(".start-rect,.start-line"));
      };

      var displayZoomIn = function (startX,endX){
          if (selectRange == null) return false;
          var positionX = selectRange.middlePosition;
          var operationWrapper = d3.select(".chart-control-panel .operation-wrapper");
          operationWrapper.style("left",positionX+"px");
          operationWrapper.classed("zoom-in",true);
          operationWrapper.classed("zoom-out",false);
      };
 
      var hideZoom = function(){
          var operationWrapper = d3.select(".chart-control-panel .operation-wrapper");
          operationWrapper.classed("zoom-in",false);
          operationWrapper.classed("zoom-out",false);
      };
  
      /**
      * function bind click event to zomm in icon
      **/
      var bindZoomInEvent = function(){
          return false;
          // d3.select(".chart-control-panel .operation-wrapper .glyphicon-zoom-in").on("click",function(){
          //         for(var k in data){
          //             data[k] = data[k].splice(selectRange.from,selectRange.to - selectRange.from + 1);
          //         }
          //         codinateXLabels = codinateXLabels.splice(selectRange.from,selectRange.to - selectRange.from + 1);
          //         $scope.$emit("scope.data.update");

          // });
      };

      var setupAgentsObj = function(){
         $scope.agentsObjs = [];
         angular.forEach($scope.agents,function(value,key){
              $scope.agentsObjs.push({
                 name:value,
                 selected:false
              });
         });
      };


      var display = function(index){
          if (index=="all") {
            callChart.selectAll("path").classed("hideChartElement",false);
            callChart.selectAll(".dots circle").classed("hideChartElement",false);
            return ;
          };

          /**
          * hide all other line
          **/
          var pathClass=".path"+index;
          var circleClass = ".dot-"+index;
          callChart.selectAll("path:not("+pathClass+")").classed("hideChartElement",true);
          callChart.selectAll(".dots circle:not("+circleClass+")").classed("hideChartElement",true);
          callChart.selectAll(pathClass).classed("hideChartElement",false);
          callChart.selectAll(circleClass).classed("hideChartElement",false);
      };


      var initChartBaseOnData=function (extenalData){
          drawBackgroundLinesAndText(extenalData);
          drawDataPath(extenalData);
          drawBars(extenalData);
          drawExtentChart(extenalData);
                   
      }

      var bindWindowScaleEvents = function(){
          //return false;
          d3.select(window).on("resize",function(e){
              /**
              * get window width and height
              **/
              var winW = window.innerWidth;
                             
              if(winW < svgWidth){
                 var ratio = winW/svgWidth;
                 var scaleString = "scale("+ratio+")"
                 var transformString = "transform:"+scaleString+";";
                 transformString+="-webkit-transform:"+scaleString+";";
                 transformString+="-moz-transform:"+scaleString+";";
                 transformString+="-ms-transform:"+scaleString+";";
                 transformString+="transform-origin:0 0;";
                 
                 chartWrapper
                 .attr("style",transformString);
              }

          });
      };

      var init = function(extenalData){
           initDataAndScale(extenalData);
           buildSvg();
           buildGroups();
           buildCallDetailPop();
           buildSelectArea();
           //bindZoomInEvent();
           initChartBaseOnData(data);
           drawExtent(extenalData);
           bindWindowScaleEvents();
           /**
           * add drag event to whole svg
           **/
           //callChart.call(drag);
      }

      init($scope.data);
      
      $scope.toggleLine = function(obj,index){
         /**
         * set all other agent inactive
         **/
         angular.forEach($scope.agentsObjs,function(value,key){
             if(key != index){
                 value.selected = false;
             }
         });
         obj.selected = !obj.selected;
         obj.selected ? display(index) : display("all");

      };

        /**
        * watch the data change
        **/
        $scope.$watch("data",function(newVal,oldVal){
            if(newVal.toString() == oldVal.toString()){
              return false;
            }
            console.log("data change",$scope.data);
            initDataAndScale($scope.data);
            initChartBaseOnData($scope.data);

        },true);

       $scope.$watch("agents",function(newVal,oldVal){
            console.log("agents change, new value is:",newVal);
            setupAgentsObj();
       },true);

       $scope.$watch("codinateXLabels",function(newVal,oldVal){
            console.log("value of codinateXLabels",newVal,oldVal);
            initXais();
       },true);


       $scope.$on("scope.data.update",function(event,msg){
           initDataAndScale(data);
           initChartBaseOnData(data);
           initSelectArea();
       });

     }
    }

});
