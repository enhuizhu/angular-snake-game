"use strict";

angular.module("chart").classy.controller({
	name:"main",
	
	inject:['$scope'],
	
	init:function(){
		this.$.timeRange = {from:"00:00",to:"24:00"};
		//this.$.codinateXlabels = ["week1","week2","week3","week4","week5","week6","week7","week8"];
		this.$.codinateXlabels = ["2014-03-15","2014-03-16","2014-03-17","2014-03-18","2014-03-19","2014-03-20","2014-03-21","2014-03-22"];
		
		this.$.callData = [
		];

		//this.$.callData.push(this.$.addAgents("get"));

		this.$.agents=["agent"];

        this.generateTotalAndCompeleteCalls();
		
        this.$.pieChart = [
           {label:"agent1",count:10},
           {label:"agent2",count:20}
        ];

        /**
        * bind calendar events
        **/
        
        jQuery(document).ready(function(){
        	jQuery( "#startDatepicker, #endDatepicker" ).datepicker({   
                    dateFormat: "yy-mm-dd"
             });
        });


	},

  
	methods:{

	  generateTotalAndCompeleteCalls:function(){
        //this.$.total.totalCalls
        var that = this;
        
        /**
        * define the value of total
        **/
        this.$.total = {
           totalCalls:[],
           compelete:[]
        };


        this.$.callData.map(function(v,index){
             v.map(function(v2,index2){
                  if(!that.$.total.totalCalls[index2]){
                  	 that.$.total.totalCalls[index2] = 0;
                  }
                  that.$.total.totalCalls[index2]+=v2;
             });             
        });

        this.$.total.totalCalls.map(function(v,i){
            that.$.total.compelete[i] = Math.round(Math.random()*v);	
        }); 

        console.log("the value of total is:",this.$.total);  

     },	

	  updateData:function(){
        console.log("update the chart data");
        this.$.callData = [[20,20,30,40,50,60,70,80]];  
        //this.$.callData[0][0] = 40;
      },
 
      addAgents:function(get){
          if(this.$.callData.length >=7 && !get) return false;
          var data = [];
          for(var i=0;i<this.$.codinateXlabels.length;i++){
             var y = 0;
             switch(i){
             	case 0:
             	case this.$.codinateXlabels.length-1:
             	   y = 0;
             	   break;
             	default:
             	   y = Math.round(Math.random()*100)
             }
             data.push(y);
          }
          if(get) return data;
          this.$.callData.push(data);
          this.$.agents.push("agent"+this.$.callData.length);
          this.generateTotalAndCompeleteCalls();
      },

      removeAgents:function(){
          this.$.callData.pop();
          this.$.agents.pop();
      },

      addPieData:function(){
      	  var label = "agent" + (this.$.pieChart.length + 1);
      	  var value = Math.round(Math.random()*100);
      	  this.$.pieChart.push({label:label,count:value});
      },

      removePieData:function(){
          this.$.pieChart.pop();
      },

      changeCodinateXlabels : function(){
           this.$.codinateXlabels = ["2014-03-15","2014-03-16"];
      },

      getNewData : function(){
           var startDate = new Date(jQuery("#startDatepicker").val());
           var endDate = new Date(jQuery("#endDatepicker").val());
           var timeGap = jQuery("#timegap").val();
           
           var hour = 60*60*1000;
           var day = hour * 24;
           var month = day*30;
           var timegap = 0;

           if(timeGap==0){
           	  alert("timegap can not be 0");
           	  return false;
           }

           switch(timeGap){
           	   case "month":
           	        timegap = month;
           	        break;
           	   case "day":
           	        timegap = day;
           	        break;
           	   case "hour":
           	        timegap = hour;
           	        break;
           }

           if(endDate.getTime() - startDate.getTime() < 0){
           	  alert("end date must be later then start date");
           	  return false;
           }



           var points = Math.floor((endDate.getTime() - startDate.getTime())/timegap);
           
           console.log("the value of points is:",points);
         
           
           var codinateXlabels = [];
           
           codinateXlabels.push(startDate.getTime());
            for(var i=1;i<=points;i++){
            	
                codinateXlabels.push(startDate.getTime()+timegap*i - timegap/2);
            	// if(i==1){
            	// 	codinateXlabels.push(startDate.getTime()+timegap*i-time);
            	// }else{
            	// 	codinateXlabels.push(startDate.getTime()+timegap*i);
            	// }
                
            };

           codinateXlabels.push(endDate.getTime());
   
           this.$.codinateXlabels = codinateXlabels;

           var newCallData = [];
           
           for(var i=0; i< this.$.agents.length;i++){
           	   newCallData.push(this.$.addAgents("get"));
           }

           this.$.callData = newCallData;
           this.generateTotalAndCompeleteCalls();
           console.log(startDate,endDate,timeGap,timegap);



      }

	}

});